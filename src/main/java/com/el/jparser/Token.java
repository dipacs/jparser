package com.el.jparser;

/**
 *
 * @author dipacs
 */
public class Token implements ITextRange {
    
    private final ATokenMatcher matcher;
    private final int startPos;
    private final int endPos;
    private final String image;
    private final boolean skipped;

    public Token(ATokenMatcher matcher, int startPos, int endPos, String image, boolean skipped) {
        this.matcher = matcher;
        this.startPos = startPos;
        this.endPos = endPos;
        this.image = image;
        this.skipped = skipped;
    }

    public ATokenMatcher getMatcher() {
        return matcher;
    }

    @Override
    public int getStartPos() {
        return startPos;
    }

    @Override
    public int getEndPos() {
        return endPos;
    }

    public String getImage() {
        return image;
    }

    public boolean isSkipped() {
        return skipped;
    }
    
    
}
