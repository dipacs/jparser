
package com.el.jparser;

/**
 *
 * @author dipacs
 */
public interface ITextRange {
    
    public int getStartPos();
    public int getEndPos();
    
}
