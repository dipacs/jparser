package com.el.jparser;

import java.util.List;

/**
 *
 * @author dipacs
 * @param <Result>
 */
public class JParser<Result extends AAstItem> {
    
    private final ConstantTokenMatcher[] constantTokenMatchers;
    private final ATokenMatcher[] dynamicTokenMatchers;
    private final AParser<Result> parser;

    public JParser(ConstantTokenMatcher[] constantTokenMatchers, ATokenMatcher[] dynamicTokenMatchers, AParser<Result> parser) {
        this.constantTokenMatchers = constantTokenMatchers;
        this.dynamicTokenMatchers = dynamicTokenMatchers;
        this.parser = parser;
    }
    
    public final ParserResult<Result> parse(String text) {
        CharStream charStream = new CharStream(text);
        TokenManager tokenManager = new TokenManager(charStream, constantTokenMatchers, dynamicTokenMatchers);
        ParserContext ctx = new ParserContext();
        Result result = parser.parse(tokenManager, ctx);
        return new ParserResult<>(result, ctx.getSyntaxErrors());
    }
    
    public static class ParserResult<Result extends AAstItem> {
        
        private final Result result;
        private final List<SyntaxError> errors;

        public ParserResult(Result result, List<SyntaxError> errors) {
            this.result = result;
            this.errors = errors;
        }

        public Result getResult() {
            return result;
        }

        public List<SyntaxError> getErrors() {
            return errors;
        }
        
    }
    
}
