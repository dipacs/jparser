
package com.el.jparser;

/**
 *
 * @author dipacs
 */
public final class SyntaxError {
    
    private final int startPos;
    private final int endPos;
    private final String message;
    
    public SyntaxError(ITextRange item, String message) {
        this(item.getStartPos(), item.getEndPos(), message);
    }
    
    public SyntaxError(ITextRange startItem, ITextRange endItem, String message) {
        this(startItem.getStartPos(), endItem.getEndPos(), message);
    }

    public SyntaxError(int startPos, int endPos, String message) {
        this.startPos = startPos;
        this.endPos = endPos;
        this.message = message;
    }

    public int getStartPos() {
        return startPos;
    }

    public int getEndPos() {
        return endPos;
    }

    public String getMessage() {
        return message;
    }
    
}
