
package com.el.jparser;

/**
 *
 * @author dipacs
 */
public enum EParserErrorType {
    
    ERROR,
    WARNING,
    INFO;
    
}
