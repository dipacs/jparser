
package com.el.jparser;

/**
 *
 * @author dipacs
 */
public class ParserError {
    
    private final int startPos;
    private final int endPos;
    private final EParserErrorType type;
    private final String message;
    
    public ParserError(AAstItem item, EParserErrorType type, String message) {
        this(item.getStartPos(), item.getEndPos(), type, message);
    }
    
    public ParserError(AAstItem startItem, AAstItem endItem, EParserErrorType type, String message) {
        this(startItem.getStartPos(), endItem.getEndPos(), type, message);
    }

    public ParserError(int startPos, int endPos, EParserErrorType type, String message) {
        if (startPos < 0) {
            throw new IllegalArgumentException("The startPos parameter can not be less than zero.");
        }
        if (endPos <= startPos) {
            throw new IllegalArgumentException("The endPos parameter must be greater than the startPos parameter.");
        }
        if (type == null) {
            throw new NullPointerException("The type parameter can not be null.");
        }
        if (message == null) {
            throw new NullPointerException("The type parameter can not be null.");
        }
        this.startPos = startPos;
        this.endPos = endPos;
        this.type = type;
        this.message = message;
    }

    public int getStartPos() {
        return startPos;
    }

    public int getEndPos() {
        return endPos;
    }

    public EParserErrorType getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }
    
}
