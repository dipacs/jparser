
package com.el.jparser;

/**
 *
 * @author dipacs
 */
public interface ILookupItem {
    
    public boolean lookup(TokenManager tm);
    
}
