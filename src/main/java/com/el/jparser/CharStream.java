package com.el.jparser;

/**
 *
 * @author dipacs
 */
public class CharStream {
    
    private final char[] chars;
    private int position = 0;
    
    public CharStream(String text) {
        this(text.toCharArray(), 0);
    }
    
    private CharStream(char[] chars, int pos) {
        this.chars = chars;
        this.position = pos;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
    
    public CharStream createNew() {
        return new CharStream(chars, position);
    }
    
    public char lookupChar() {
        if (isEndOfFile()) {
            throw new RuntimeException("End of file reached.");
        }
        return chars[position];
    }
    
    public char reedChar() {
        if (isEndOfFile()) {
            throw new RuntimeException("End of file reached.");
        }
        return chars[position++];
    }
    
    public boolean match(char c) {
        if (isEndOfFile()) {
            return false;
        }
        if (c != chars[position]) {
            return false;
        }
        position++;
        return true;
    }
    
    public boolean matchAny(char[] chars) {
        if (isEndOfFile()) {
            return false;
        }
        for (char c : chars) {
            if (c == this.chars[position]) {
                position++;
                return true;
            }
        }
        return false;
    }
    
    public String getString(int startPos, int count) {
        return new String(chars, startPos, count);
    };
    
    public boolean match(char[] characters) {
        for (int i = 0; i < characters.length; i++) {
            if (position + i >= chars.length) {
                return false;
            }
            if (characters[i] != chars[position + i]) {
                return false;
            }
        }
        position += characters.length;
        return true;
    }
    
    public boolean isEndOfFile() {
        return this.position >= chars.length;
    }
    
}
