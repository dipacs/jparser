package com.el.jparser;

/**
 *
 * @author dipacs
 */
public abstract class AParser<Result extends AAstItem> implements ILookupItem {
    
    private final String name;

    public AParser(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public abstract boolean lookup(TokenManager tokenManager);
    
    public abstract Result parse(TokenManager tokenManager, ParserContext ctx);
    
}
