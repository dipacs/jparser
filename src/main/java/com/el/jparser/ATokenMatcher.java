package com.el.jparser;

/**
 *
 * @author dipacs
 */
public abstract class ATokenMatcher implements ILookupItem {
    
    private final String name;
    private final boolean skip;

    public ATokenMatcher(String name) {
        this(name, false);
    }

    public ATokenMatcher(String name, boolean skip) {
        this.name = name;
        this.skip = skip;
    }

    public String getName() {
        return name;
    }

    public boolean isSkip() {
        return skip;
    }
    
    public abstract int match(CharStream chars);

    @Override
    public boolean lookup(TokenManager tm) {
        return tm.lookupToken().getMatcher() == this;
    }
    
}
