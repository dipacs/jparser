package com.el.jparser;

/**
 *
 * @author dipacs
 */
public class AAstItem implements ITextRange {
    
    private final int startPos;
    private final int endPos;
    
    public AAstItem(ITextRange startItem, ITextRange endItem) {
        this(startItem.getStartPos(), endItem.getEndPos());
    }

    public AAstItem(int startPos, int endPos) {
        this.startPos = startPos;
        this.endPos = endPos;
    }

    @Override
    public int getStartPos() {
        return startPos;
    }

    @Override
    public int getEndPos() {
        return endPos;
    }
    
}
