package com.el.jparser;

/**
 *
 * @author dipacs
 */
public class ConstantTokenMatcher extends ATokenMatcher {
    
    private final char[] chars;

    public ConstantTokenMatcher(String name, String value) {
        super(name);
        this.chars = value.toCharArray();
    }

    public ConstantTokenMatcher(String name, char[] chars) {
        super(name);
        if (chars.length < 1) {
            throw new IllegalArgumentException("The chars parameter can not be empty.");
        }
        this.chars = chars;
    }
    
    public String getImage() {
        return new String(chars);
    }

    @Override
    public int match(CharStream stream) {
        if (stream.match(chars)) {
            return chars.length;
        }
        return 0;
    }
    
}
