
package com.el.jparser;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dipacs
 */
public final class ParserContext {
    
    private final List<SyntaxError> syntaxErrors = new ArrayList<>();

    public List<SyntaxError> getSyntaxErrors() {
        return syntaxErrors;
    }
    
    public void addError(SyntaxError syntaxError) {
        if (syntaxError == null) {
            throw new NullPointerException("The 'syntaxError' parameter can not be null.");
        }
        
        syntaxErrors.add(syntaxError);
    }
    
    public void addError(ITextRange startItem, ITextRange endItem, String required, String found) {
        addError(new SyntaxError(startItem, endItem, "" + required + " is required but " + found + " is found."));
    }
    
    public void addError(ITextRange startItem, ITextRange endItem, ATokenMatcher required, ATokenMatcher found) {
        // TODO fix enclosing braces
        addError(startItem, endItem, "<" + required.getName() + ">", "<" + found.getName() + ">");
    }
    
    public void addError(ITextRange startItem, ITextRange endItem, String required, ATokenMatcher found) {
        // TODO fix enclosing braces
        addError(startItem, endItem, required, "<" + found.getName() + ">");
    }
    
    public void addError(ITextRange startItem, ITextRange endItem, ATokenMatcher required, String found) {
        // TODO fix enclosing braces
        addError(startItem, endItem, "<" + required.getName() + ">", found);
    }
    
}
