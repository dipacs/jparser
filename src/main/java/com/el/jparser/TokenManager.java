package com.el.jparser;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dipacs
 */
public class TokenManager {

    public static final EofMatcher EOF_MATCHER = new EofMatcher();
    private final Token[] tokens;
    private int position;

    public TokenManager(CharStream charStream, ConstantTokenMatcher[] constantMatchers, ATokenMatcher[] dinamicMatchers) {
        List<Token> tokensList = new ArrayList<>(1000);
        Token token = null;
        do {
            token = parseNextToken(charStream, constantMatchers, dinamicMatchers);
            if (token != null) {
                tokensList.add(token);
            }
        } while (token != null);
        Token eofToken = new Token(EOF_MATCHER, charStream.getPosition() - 1, charStream.getPosition(), "<EOF>", false);
        tokensList.add(eofToken);
        tokens = tokensList.toArray(new Token[tokensList.size()]);
    }

    private Token parseNextToken(CharStream charStream, ConstantTokenMatcher[] constantMatchers, ATokenMatcher[] dinamicMatchers) {
        boolean isEnd = false;
        Token res = null;
        while (!isEnd) {
            if (charStream.isEndOfFile()) {
                return null;
            }
            Token constantToken = getNextToken(charStream, constantMatchers);
            Token dynamicToken = getNextToken(charStream, dinamicMatchers);

            if (constantToken != null && dynamicToken != null) {
                int constantTokenLength = constantToken.getEndPos() - constantToken.getStartPos();
                int dynamicTokenLength = dynamicToken.getEndPos() - dynamicToken.getStartPos();
                if (constantTokenLength >= dynamicTokenLength) {
                    res = constantToken;
                } else {
                    res = dynamicToken;
                }
            } else if (constantToken != null) {
                res = constantToken;
            } else if (dynamicToken != null) {
                res = dynamicToken;
            } else {
                throw new RuntimeException("No matching token can be found.");
            }

            charStream.setPosition(charStream.getPosition() + (res.getEndPos() - res.getStartPos()));

            if (!res.isSkipped()) {
                isEnd = true;
            }
        }

        return res;
    }

    public boolean isEndReached() {
        return position >= tokens.length;
    }

    public Token lookupToken() {
        return lookupToken(false);
    }

    public Token lookupToken(boolean includeSkipped) {
        if (isEndReached()) {
            throw new RuntimeException("End of file reached.");
        }
        if (includeSkipped) {
            return tokens[position];
        } else {
            Token res = null;
            int pos = position;
            do {
                Token t = tokens[pos++];
                if (!t.isSkipped()) {
                    res = t;
                }
            } while (res == null);
            return res;
        }
    }

    public Token getNextToken() {
        return getNextToken(false);
    }

    public Token getNextToken(boolean includeSkipped) {
        if (isEndReached()) {
            throw new RuntimeException("End of file reached.");
        }
        if (includeSkipped) {
            Token res = tokens[position];
            if (res.getMatcher() != EOF_MATCHER) {
                position++;
            }
            return res;
        } else {
            Token res = null;
            do {
                Token t = tokens[position];
                position++;
                if (!t.isSkipped()) {
                    res = t;
                }
            } while (res == null);
            return res;
        }
    }

    private Token getNextToken(CharStream charStream, ATokenMatcher[] matchers) {
        int maxCount = 0;
        ATokenMatcher maxMatcher = null;
        for (ATokenMatcher matcher : matchers) {
            int count = matcher.match(charStream.createNew());
            if (count > maxCount) {
                maxCount = count;
                maxMatcher = matcher;
            }
        }
        if (maxMatcher != null) {
            int startPos = charStream.getPosition();
            int endPos = startPos + maxCount;
            String image = charStream.getString(startPos, maxCount);
            return new Token(maxMatcher, startPos, endPos, image, maxMatcher.isSkip());
        }

        return null;
    }

    public Token[] lookupTokens(int count) {
        return lookupTokens(count, false);
    }

    public Token[] lookupTokens(int count, boolean includeSkipped) {
        int startPos = position;
        try {
            Token[] res = new Token[count];
            for (int i = 0; i < count; i++) {
                res[i] = getNextToken(includeSkipped);
            }
            return res;
        } finally {
            this.position = startPos;
        }
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
    
    public void stepBack(int count) {
        stepBack(count, false);
    }
    
    public void stepBack(int count, boolean includeSkipping) {
        if (count < 0) {
            throw new IllegalArgumentException("Invalid 'count' parameter. Can not be negative.");
        }
        if (count == 0) {
            return;
        }
        if (includeSkipping) {
            if (count > position) {
                throw new IllegalArgumentException("Invalid 'count' parameter. Not enogh tokens to step back.");
            }
            
            position = position - count;
        }
        int backed = 0;
        int newPos = position;
        while (backed < count) {
            if (newPos == 0) {
                throw new IllegalArgumentException("Invalid 'count' parameter. Not enough tokens to step back.");
            }
            Token t = tokens[newPos - 1];
            if (!t.isSkipped()) {
                backed++;
            }
            newPos--;
        }
        position = newPos;
    }
    
    public boolean forward(ParserContext ctx, ILookupItem... lookups) {
        if (lookups == null) {
            throw new NullPointerException("The 'lookups' parameter can not be null.");
        }
        
        if (lookups.length == 0) {
            throw new IllegalArgumentException("Minimum 1 lookup item is needed.");
        }
        
        int startPos = position;
        boolean res = false;
        while (!isEndReached()) {
            boolean found = false;
            for (ILookupItem item : lookups) {
                if (item.lookup(this)) {
                    found = true;
                    break;
                }
            }
            if (found) {
                res = true;
                break;
            } else {
                Token t = lookupToken(true);
                if (!t.isSkipped()) {
                    if (t.getMatcher() instanceof ConstantTokenMatcher) {
                        
                    } else {
                        String required = buildRequiredString(lookups);
                        ctx.addError(t, t, required, "<" + t.getMatcher().getName() + ">");
                    }
                }
            }
            position++;
        }
        
        if (!res) {
            position = startPos;
        }
        return res;
    }
    
    private String buildRequiredString(ILookupItem... lookups) {
        String res = "";
        boolean isFirst = true;
        for (ILookupItem item : lookups) {
            if (isFirst) {
                isFirst = false;
            } else {
                res += " or ";
            }
            if (item instanceof AParser) {
                res += "[" + ((AParser)item).getName() + "]";
            } else if (item instanceof ConstantTokenMatcher) {
                res += "'" + ((ConstantTokenMatcher)item).getImage() + "'";
            } else if (item instanceof ATokenMatcher) {
                res += "<" + ((ATokenMatcher)item).getName() + ">";
            }
        }
        return res;
    }

    private static final class EofMatcher extends ATokenMatcher {

        public EofMatcher() {
            super("<EOF>");
        }

        @Override
        public int match(CharStream chars) {
            return 1;
        }

    }

}
