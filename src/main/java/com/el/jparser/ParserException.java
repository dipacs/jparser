package com.el.jparser;

/**
 *
 * @author dipacs
 */
public class ParserException extends Exception {
    
    private final int startPos;
    private final int endPos;

    public ParserException(int startPos, int endPos, String message) {
        super(message);
        this.startPos = startPos;
        this.endPos = endPos;
    }

    public int getStartPos() {
        return startPos;
    }

    public int getEndPos() {
        return endPos;
    }
    
}
